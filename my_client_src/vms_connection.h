#pragma once

#include <boost/asio.hpp>

#include <memory>



namespace
{
	using boost::asio::ip::tcp;
}


class Client;
struct VmsConnection : public std::enable_shared_from_this<VmsConnection>
{
	VmsConnection(tcp::socket socket, Client* parent);
	void Start();
	~VmsConnection();
	void do_read();
	void do_write(size_t len);
	void forward_response(int status, const uint8_t* data, size_t datalen);

	//private:
	tcp::socket socket_;
	Client* parent_;
	enum {MAX_LENGTH = 65536};
	char data_[MAX_LENGTH];
	boost::asio::streambuf http2_data_buffer_;
	int32_t http2_session_id_; // TODO: rename on http2_stream_id_;
	int32_t response_status_code_ = 200; // let's use 200 as a default
};



