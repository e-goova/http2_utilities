#include "vms_connection.h"

#include "my_client.h"

#include <boost/beast/http.hpp>

#include <iostream>


VmsConnection::VmsConnection(tcp::socket socket, Client* parent)
	: socket_(std::move(socket))
	, parent_(parent)
{
}

void VmsConnection::Start()
{
	do_read();
};

VmsConnection::~VmsConnection()
{
	std::cout << __FUNCTION__ << std::endl;
}

void VmsConnection::do_read()
{
	socket_.async_read_some(boost::asio::buffer(data_, MAX_LENGTH),
		[self = shared_from_this()](boost::system::error_code ec, std::size_t length)
	{
		if(!ec)
		{
			std::cout << "Received request to forward with len " << length << std::endl;
			self->http2_session_id_ = self->parent_->do_write_request((const uint8_t*)self->data_, length);
			return self->do_read();
		}
		else if(ec == boost::asio::error::eof)
		{
			std::cout << "VMS connection closed on: " << self->socket_.remote_endpoint().address().to_string()
				<< ":" << self->socket_.remote_endpoint().port() << std::endl;
		}
		else
		{
			std::cout << "Error while reading on socket " << self->socket_.remote_endpoint().address().to_string()
				<< ":" << self->socket_.remote_endpoint().port() << ". Errc and msg: " << ec.value() << " " << ec.message() << std::endl;
		}

		self->parent_->remove_vms_connection(self->http2_session_id_);
	});
}

void VmsConnection::forward_response(int status, const uint8_t* data, size_t datalen)
{
	std::cout << "Received data to forward with datalen: " << datalen << std::endl;

	// Set up the response
	using namespace boost::beast;
	auto res = std::make_shared<http::response<http::string_body>>();
	res->version(11);
	res->set(http::field::server, "MUX/DEMUX service");
	res->result(status);
	res->set(http::field::content_type, "application/soap+xml; charset=utf-8;");

	if(datalen)
	{
		res->set(http::field::content_length, datalen);
		res->body().assign((const char*)data, datalen);
	}

	http::async_write(socket_, *res, [res](error_code ec, size_t bytes_transf)
	{
		if(ec)
			std::cerr << "An error occurred while forwarding a response" << std::endl;
		else
		{
			std::cout << "Forwarded response in size: " << bytes_transf	 << std::endl;
		}
	});
}
