#include <nghttp2/nghttp2.h>

#include "my_client.h"

#include <boost/system/error_code.hpp>
#include <boost/asio.hpp>
#include <boost/beast/http.hpp>


#include <iostream>
#include <memory>
#include <utility>
#include <functional>





Client::Client(boost::asio::io_context& io_context, short http2_port, short requests_listening_port)
	: io_context_(io_context)
	, acceptor_(io_context_, tcp::endpoint(tcp::v4(), http2_port))
	, requests_listening_port_acceptor_(io_context, tcp::endpoint(tcp::v4(), requests_listening_port))
{
	std::cout << "Listening for incoming HTTP/2 connection on port: " << http2_port
		<< "\nListening for requests on port: " << requests_listening_port << std::endl;

	do_accept();
}


void Client::do_accept()
{
	acceptor_.async_accept(
		[this](boost::system::error_code ec, tcp::socket socket)
	{
		if(!ec)
		{
			std::cout << "HTTP/2 TCP connection accepted from " << socket.remote_endpoint().address().to_string()
				<< ":" << socket.remote_endpoint().port() << std::endl;
			socket_.reset(new boost::asio::ip::tcp::socket(std::move(socket)));

			init_nghttp2();
			open_http2_connection();

			do_read();

			do_requests_port_accept();
		}
	}
	);
}

void Client::do_requests_port_accept()
{
	std::cout << "Start listening for a requests connection" << std::endl;
	requests_listening_port_acceptor_.async_accept(
		[this](boost::system::error_code ec, tcp::socket socket)
	{
		if(!ec)
		{
			std::cout << "New request connection accepted from " << socket.remote_endpoint().address().to_string()
				<< ":" << socket.remote_endpoint().port() << std::endl;

			auto rc = std::make_shared<VmsConnection>(std::move(socket), this);
			rc->Start();
			rconnections.push_back(rc);
		}

		do_requests_port_accept();
	}
	);
}


void Client::init_nghttp2()
{
	std::cout << "Initialize nghttp2" << std::endl;

	nghttp2_session_callbacks* callbacks;
	nghttp2_session_callbacks_new(&callbacks);
	nghttp2_session_callbacks_set_send_callback(callbacks, send_callback);
	nghttp2_session_callbacks_set_on_frame_recv_callback(callbacks, on_frame_recv_callback);
	nghttp2_session_callbacks_set_on_header_callback(callbacks, on_header_recv_callback);
	nghttp2_session_callbacks_set_on_data_chunk_recv_callback(callbacks, on_data_chunk_recv_callback);
	nghttp2_session_client_new(&http2_session_, callbacks, this);
	nghttp2_session_callbacks_del(callbacks);
}

void Client::open_http2_connection()
{
	send_client_connection_header();
	//send_client_connect_method();
}

void Client::send_client_connection_header()
{
	nghttp2_settings_entry iv[1] =
	{
		{NGHTTP2_SETTINGS_MAX_CONCURRENT_STREAMS, 100}
	};

	int rv;
	/* client 24 bytes magic string will be sent by nghttp2 library */
	rv = nghttp2_submit_settings(http2_session_, NGHTTP2_FLAG_NONE, iv, ARRLEN(iv));

	if(rv != 0)
	{
		std::cout << "Could not submit SETTINGS: " << nghttp2_strerror(rv) << std::endl;
		throw std::runtime_error("Error while submitting settings");
	}

	rv = session_send();

	if(rv != 0)
	{
		std::cout << "session_send() error: " << nghttp2_strerror(rv) << std::endl;
		throw std::runtime_error("Error while setting session settings");
	}
}

int Client::session_send()
{
	int rv;
	rv = nghttp2_session_send(http2_session_);

	if(rv != 0)
	{
		std::cerr << "Fatal error: " << nghttp2_strerror(rv) << std::endl;
		return -1;
	}

	return 0;
}

void Client::do_read()
{
	socket_->async_read_some(boost::asio::buffer(data_, MAX_LENGTH),
		[this](boost::system::error_code ec, std::size_t length)
	{
		if(ec)
		{
			std::cout << "Reading finished with an error" << std::endl;
			return;
		}

		std::cout << "Received data in bytes: " << length << std::endl;
		ssize_t readlen = nghttp2_session_mem_recv(http2_session_, (const uint8_t*)data_, length);

		if(readlen < 0)
		{
			std::cout << "Error: " << nghttp2_strerror((int)readlen) << std::endl;
			//delete_http2_session_data(session_data);
			return;
		}

		do_read();
	}
	);
}

// Returns a created stream's ID
int32_t Client::do_write_request(const uint8_t* data, size_t len)
{
	boost::beast::http::request_parser<boost::beast::http::string_body> http_parser;

	boost::beast::error_code ec;
	auto consumed = http_parser.put(boost::asio::buffer(data, len), ec);

	// TODO: add @ec handling correctly. There could be an error because not enough data received
	if(ec) throw std::runtime_error("parsing HTTP 1.1 header error");

	// now parse http body
	consumed = http_parser.put(boost::asio::buffer(data + consumed, len - consumed), ec);

	if(ec) throw std::runtime_error("parsing HTTP 1.1 body error");

	if(!http_parser.is_done() || !http_parser.is_header_done())
		throw std::runtime_error("parsing HTTP 1.1 request is not done!");

	nghttp2_nv hdrs[] =
	{
		MAKE_NV2(":method", "POST"),
		MAKE_NV2(":scheme", "http"),
		//MAKE_NV2(":protocol", "websocket"),
		//MAKE_NV2(":path", http_parser.get().target().data()),
		{(uint8_t*)":path", (uint8_t*)http_parser.get().target().data(), 5, http_parser.get().target().length()},
		MAKE_NV2("host", "192.168.42.170:13080")
	};

	// TODO: free memory
	auto* mydsptr = new MyDataSource;
	mydsptr->dataLen = http_parser.get().body().length();
	mydsptr->data = (const uint8_t*)http_parser.get().body().data();

	nghttp2_data_source dsource;
	dsource.ptr = mydsptr;

	nghttp2_data_provider dprd;
	dprd.source = dsource;
	dprd.read_callback = &on_data_source_read_callback;

	int32_t stream_id = nghttp2_submit_request(http2_session_, NULL, hdrs, ARRLEN(hdrs), &dprd, this);

	if(stream_id < 0)
	{
		std::cout << "Could not submit HTTP request: " << nghttp2_strerror(stream_id) << std::endl;
		return stream_id;
	}

	if(session_send() != 0)
	{
		//delete_http2_session_data(session_data);
		throw std::runtime_error("Session send finished with an error");
	}

	return stream_id;
}

void Client::remove_vms_connection(int32_t http2_session_id)
{
	rconnections.erase(std::remove_if(rconnections.begin(), rconnections.end(),
			[http2_session_id](std::shared_ptr<VmsConnection>& vc)
	{
		return vc->http2_session_id_ == http2_session_id;
	})
	);
}

ssize_t send_callback(nghttp2_session* session, const uint8_t* data,	size_t length, int flags, void* user_data)
{
	std::cout << "nghttp2 wanna write something with len: " << length << std::endl;

	auto* client_instance = (Client*) user_data;
	boost::asio::async_write(*client_instance->socket_, boost::asio::buffer(data, length),
		[client_instance](boost::system::error_code ec, std::size_t length)
	{
		std::cout << "Send through send_callback: " << length << std::endl;

		if(ec)
			std::cout << "Error in write" << std::endl;
	}
	);

	return (ssize_t)length;
}

int on_frame_recv_callback(nghttp2_session* session, const nghttp2_frame* frame, void* user_data)
{
	std::cout << "Frame received" << std::endl;

	auto* client_instance = (Client*) user_data;

	switch(frame->hd.type)
	{
		case NGHTTP2_SETTINGS:
		{
			std::cout << "Received SETTINGS, sending response" << std::endl;
			auto rv = client_instance->session_send();

			if(rv != 0)
			{
				std::cout << "SETTINGS response finished with an error: " << nghttp2_strerror(rv) << std::endl;
			}
		}
		break;

		case NGHTTP2_DATA:
		{
			//std::cout << "Received DATA frame" << std::endl;
			//client_instance->rconnections.back()->forward_response(frame->data, );
		}
		break;

		default:
			break;
	}

	return 0;
}

int on_header_recv_callback(nghttp2_session* /*session*/, const nghttp2_frame* frame, const uint8_t* name, size_t /*namelen*/, const uint8_t* value, size_t /*valuelen*/, uint8_t /*flags*/, void* user_data)
{
	//std::cout << "Header frame received. Name: " << (const char*)name << " value: " << (const char*)value << std::endl;

	auto client_instance = (Client*)user_data;
	auto rc_it = std::find_if(client_instance->rconnections.begin(), client_instance->rconnections.end(),
			[frame](std::shared_ptr<VmsConnection> rc)
	{
		return rc->http2_session_id_ == frame->hd.stream_id;
	});

	if(rc_it == client_instance->rconnections.end())
	{
		std::cerr << "Not found a stream ID = " << frame->hd.stream_id << std::endl;
		return 0; // should we return an error code instead 0 here???
	}

	if(strcmp((const char*)name, ":status") == 0)
	{
		(*rc_it)->response_status_code_ = atoi((const char*)value);
	}

	return 0;
}

int on_data_chunk_recv_callback(nghttp2_session* http2_session, const uint8_t flags, int32_t stream_id, const uint8_t* data, size_t length, void* user_data)
{
	auto client_instance = (Client*)user_data;

	auto it = std::find_if(client_instance->rconnections.begin(), client_instance->rconnections.end(),
			[stream_id](const std::shared_ptr<VmsConnection>& rc)
	{
		return rc->http2_session_id_ == stream_id;
	});

	if(it != client_instance->rconnections.end())
	{
		auto buf = (*it)->http2_data_buffer_.prepare(length);
		memcpy(buf.data(), data, length);
		(*it)->http2_data_buffer_.commit(length);

		if((flags & NGHTTP2_FLAG_END_STREAM) != 0)
		{
			std::cout << "on_data_chunk_recv_callback() called, incoming data len: " << length
				<< " need to forward response with status code: " << (*it)->response_status_code_
				<< ". Whole buffer size: " << (*it)->http2_data_buffer_.size() << std::endl;
			(*it)->forward_response((*it)->response_status_code_, (const uint8_t*)(*it)->http2_data_buffer_.data().data(),
				(*it)->http2_data_buffer_.size());
			(*it)->http2_data_buffer_.consume((*it)->http2_data_buffer_.size());
		}
		else
		{
			std::cout << "on_data_chunk_recv_callback() called, incoming data len: " << length
				<< " flag END_STREAM not received. Waiting for more data. Current buffer size: " << (*it)->http2_data_buffer_.size() << std::endl;
		}
	}
	else
	{
		std::cerr << "Not found a RequestConnection with HTTP/2 stream ID: " << stream_id << std::endl;
	}

	return 0;
}

ssize_t on_data_source_read_callback(nghttp2_session* /*session*/, int32_t /*stream_id*/, uint8_t* buf, size_t /*length*/,
	uint32_t* data_flags, nghttp2_data_source* source, void* /*user_data*/)
{
	std::cout << "Data source read callback called" << std::endl;

	auto myds = (MyDataSource*)source->ptr;

	memcpy(buf, myds->data, myds->dataLen);
	*data_flags = NGHTTP2_DATA_FLAG_EOF;

	return myds->dataLen;
}

int main(int argc, char* argv[])
{
	try
	{
		if(argc != 3)
		{
			std::cerr << "Usage: my-client <listen_http2_port> <listen_http_port>\n";
			return 1;
		}

		short http2_port = std::atoi(argv[1]);
		short http_port = std::atoi(argv[2]);

		boost::asio::io_context io_context;

		Client c(io_context, http2_port, http_port);

		std::thread t([&io_context]()
		{
			io_context.run();
		});

		t.join();
		c.close();
	}
	catch(std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << std::endl;
	}

	std::cout << "Finished" << std::endl;
	return 0;
}

