#include "my_server.h"

int main(int argc, char* argv[])
{
	boost::asio::io_service ioservice;
	boost::asio::io_service::work work(ioservice);

	try
	{
		if(argc != 5)
		{
			std::cerr << "Usage: my-server <http2 host address> <http2 host port> <camera ip> <camera http port>\n";
			return 1;
		}

		boost::asio::io_context io_context;

		tcp::resolver resolver(io_context);
		auto http2_host_endpoints = resolver.resolve(argv[1], argv[2]);
		auto camera_endpoints = resolver.resolve(argv[3], argv[4]);

		Server s(io_context, http2_host_endpoints, camera_endpoints);
		s.Start();
	}
	catch(std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << "n";
	}

	std::cout << "finished\n";
}


Server::Server(boost::asio::io_context& io_context,
	const tcp::resolver::results_type& endpoints,
	const tcp::resolver::results_type& camera_endpoints)
	: io_context_(io_context), socket_(io_context), camera_endpoints_(camera_endpoints)
{
	do_connect(endpoints);
}

void Server::do_connect(const tcp::resolver::results_type& endpoints)
{
	boost::asio::async_connect(socket_, endpoints,
		[this](boost::system::error_code ec, tcp::endpoint)
	{
		if(ec)
		{
			std::cout << "Failed to connect: " << ec.message() << std::endl;
			return;
		}

		std::cout << "Connected to a HTTP2 host" << std::endl;

		std::make_shared<Session>(std::move(socket_), camera_endpoints_, io_context_)->Start();
	});
}

int on_frame_recv_callback(nghttp2_session* http2_session, const nghttp2_frame* frame, void* user_data)
{
	std::cout << "Proc. incoming frame with type: " << (int)frame->hd.type << std::endl;
	Session* session_instance = (Session*)user_data;

	switch(frame->hd.type)
	{
		// case NGHTTP2_DATA:
		// {
		// 	if (frame->hd.flags & NGHTTP2_FLAG_END_STREAM) {
		// 		return my_on_request_recv(http2_session, session_instance, frame->hd.stream_id);
		// 	}
		// 	break;
		// }
		case NGHTTP2_HEADERS:

			/* Check that the client request has finished */
			if(frame->hd.flags & NGHTTP2_FLAG_END_STREAM)
			{
				return my_on_request_recv(http2_session, session_instance, frame->hd.stream_id);
			}

			break;

		default:

			break;
	}

	return 0;
}

int on_data_chunk_recv_callback(nghttp2_session* http2_session, const uint8_t flags, int32_t stream_id, const uint8_t* data, size_t length, void* user_data)
{
	std::cout << "on_data_chunk_recv_callback() called, incoming data len: " << length << std::endl;

	auto* s = (Session*)user_data;

	s->camera_socket_ = std::make_shared<tcp::socket>(s->io_context_);

	boost::asio::async_connect(*s->camera_socket_, s->camera_endpoints_,
		[s, stream_id, data, length](boost::system::error_code ec, tcp::endpoint)
	{
		if(!ec)
		{
			boost::asio::async_write(*s->camera_socket_, boost::asio::buffer(data, length),
				[s, stream_id](boost::system::error_code ec, size_t len)
			{
				if(!ec)
				{
					std::cout << "Forwarded data in size: " << len << std::endl;

					s->do_read_on_camera_socket(stream_id);
				}
			});
		}
	});

	return 0;
}

int my_on_request_recv(nghttp2_session* http2_session, Session* session_instance, int32_t stream_id)
{
	std::cout << "Incoming request" << std::endl;
	nghttp2_nv hdrs[] = {MAKE_NV(":status", "200")};

	if(send_response(http2_session, stream_id, hdrs, ARRLEN(hdrs)) != 0)
	{
		return NGHTTP2_ERR_CALLBACK_FAILURE;
	}

	return 0;
}

ssize_t on_datasource_read(nghttp2_session* Session, int32_t stream_id, uint8_t* buf, size_t length, uint32_t* data_flags, nghttp2_data_source* source, void* user_data)
{
	std::cout << "Data source read callback called. Max data lenth: " << length << std::endl;

	auto myds = (MyDataSource*)source->ptr;

	auto copyingLen = std::min(myds->dataLen - myds->copiedDataLen, length);
	memcpy(buf, myds->data + myds->copiedDataLen, copyingLen);
	myds->copiedDataLen += copyingLen;

	if(myds->dataLen == myds->copiedDataLen)
	{
		*data_flags = NGHTTP2_DATA_FLAG_EOF;
	}

	return (ssize_t)copyingLen;
}

int send_response(nghttp2_session* Session, int32_t stream_id, nghttp2_nv* nva, size_t nvlen)
{
	int rv;

	rv = nghttp2_submit_response(Session, stream_id, nva, nvlen, NULL);

	if(rv != 0)
	{
		std::cout << "Fatal error: " << nghttp2_strerror(rv) << std::endl;
		return -1;
	}

	return 0;
}

ssize_t send_callback(nghttp2_session* /*session_data*/, const uint8_t* data, size_t length, int /*flags*/, void* user_data)
{
	Session* session_instance = (Session*)user_data;

	boost::asio::async_write(session_instance->socket_,
		boost::asio::buffer(data, length),
		[self = session_instance->copy()](const boost::system::error_code & ec, std::size_t bytes_transferred)
	{
		std::cout << "Send HTTP/2 response in size: " << bytes_transferred << std::endl;

		//self->do_read();
	}
	);

	return (ssize_t)length;
}


