#pragma once

#include "session.h"

#include <nghttp2/nghttp2.h>

#include <boost/system/error_code.hpp>
#include <boost/asio.hpp>
#include <boost/beast/http.hpp>

#include <iostream>
#include <memory>
#include <utility>
#include <functional>

#define ARRLEN(x) (sizeof(x) / sizeof(x[0]))

#define MAKE_NV(NAME, VALUE)                                                   \
	{                                                                            \
	(uint8_t *)NAME, (uint8_t *)VALUE, sizeof(NAME) - 1, sizeof(VALUE) - 1,    \
		NGHTTP2_NV_FLAG_NONE                                                   \
}

namespace
{
	using boost::asio::ip::tcp;
}

ssize_t send_callback(nghttp2_session* http2_session, const uint8_t* data, size_t length, int flags, void* user_data);

int on_frame_recv_callback(nghttp2_session* http2_session, const nghttp2_frame* frame, void* user_data);
int on_data_chunk_recv_callback(nghttp2_session* http2_session, const uint8_t flags, int32_t stream_id, const uint8_t* data, size_t length, void* user_data);
int my_on_request_recv(nghttp2_session* http2_session, Session* session_instance, int32_t streamID);
ssize_t on_datasource_read(nghttp2_session* Session, int32_t stream_id, uint8_t* buf, size_t length, uint32_t* data_flags, nghttp2_data_source* source, void* user_data);

int send_response(nghttp2_session* Session, int32_t stream_id, nghttp2_nv* nva, size_t nvlen);

struct MyDataSource
{
	const uint8_t* data = nullptr;
	size_t dataLen = 0;
	size_t copiedDataLen = 0;
};

class Server
{
public:
	Server(boost::asio::io_context& io_context,
		const tcp::resolver::results_type& endpoints,
		const tcp::resolver::results_type& camera_endpoints);

	void Start()
	{
		io_context_.run();
	}

private:
	void do_connect(const tcp::resolver::results_type& endpoints);

private:
	boost::asio::io_context& io_context_;
	tcp::socket socket_;
	std::vector<std::shared_ptr<Session>> sessions_;

	tcp::resolver::results_type camera_endpoints_;
};

