#pragma once

#include <nghttp2/nghttp2.h>

#include <boost/asio.hpp>

#include <memory>

namespace
{
	using boost::asio::ip::tcp;
}

class Session
	: public std::enable_shared_from_this<Session>
{
public:
	Session(tcp::socket socket, const tcp::resolver::results_type& camera_endpoints, boost::asio::io_context& io_ctx);

	std::shared_ptr<Session> copy()
	{
		return shared_from_this();
	}

	int send_server_connection_header();
	int send_data(int32_t stream_id, const uint8_t* data, size_t datalen);
	int forward_response(int32_t stream_id, short status_code, const uint8_t* data, size_t datalen);
	/* Serialize the frame and send (or buffer) the data to	bufferevent. */
	int session_send();
	void Start();

public:
	void do_read();
	void do_write(std::size_t length);

	void do_read_on_camera_socket(int32_t stream_id);

public:
	tcp::socket socket_;
	enum { max_length = 4096 };
	char data_[max_length];

	boost::asio::streambuf innerBuffer_;

	nghttp2_session* http2_session_;

	const tcp::resolver::results_type camera_endpoints_;

	boost::asio::io_context& io_context_;

	std::shared_ptr<tcp::socket> camera_socket_;
};
