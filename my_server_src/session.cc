#include "session.h"
#include "my_server.h"

#include <boost/beast/http.hpp>

Session::Session(tcp::socket socket, const tcp::resolver::results_type& camera_endpoints, boost::asio::io_context& io_ctx)
	: socket_(std::move(socket)), camera_endpoints_(camera_endpoints), io_context_(io_ctx)
{
	std::cout << "New connection: " << socket_.remote_endpoint().address().to_string() << ":" << socket_.remote_endpoint().port() << std::endl;

	nghttp2_session_callbacks* mycallbacks;
	nghttp2_session_callbacks_new(&mycallbacks);

	nghttp2_session_callbacks_set_send_callback(mycallbacks, send_callback);


	nghttp2_session_callbacks_set_on_frame_recv_callback(mycallbacks, on_frame_recv_callback);

	nghttp2_session_callbacks_set_on_data_chunk_recv_callback(mycallbacks, on_data_chunk_recv_callback);

	nghttp2_session_server_new(&http2_session_, mycallbacks, this);

	nghttp2_session_callbacks_del(mycallbacks);
}

int Session::send_server_connection_header()
{
	nghttp2_settings_entry iv[2] =
	{
		{NGHTTP2_SETTINGS_MAX_CONCURRENT_STREAMS, 100},
		{NGHTTP2_SETTINGS_ENABLE_CONNECT_PROTOCOL, 1}
	};

	int rv;

	rv = nghttp2_submit_settings(http2_session_, NGHTTP2_FLAG_NONE, iv, ARRLEN(iv));

	if(rv != 0)
	{
		std::cerr << "Fatal error: " << nghttp2_strerror(rv);
		return -1;
	}

	return 0;
}

int Session::send_data(int32_t stream_id, const uint8_t* data, size_t datalen)
{
	nghttp2_data_provider data_prd;

	nghttp2_data_source dsrc;

	MyDataSource dsrcPtr;
	dsrcPtr.data = data;
	dsrcPtr.dataLen = datalen;

	dsrc.ptr = &dsrcPtr;
	dsrc.fd = 0;

	data_prd.source = dsrc;
	data_prd.read_callback = &on_datasource_read;

	int rv = nghttp2_submit_data(http2_session_, NGHTTP2_FLAG_END_STREAM, stream_id, &data_prd);

	if(rv != 0)
	{
		std::cout << "Submitting data finished with an error: " << nghttp2_strerror(rv);
		return rv;
	}
	else
	{
		std::cout << "Submitting data finished OK" << std::endl;
	}

	rv = session_send();

	if(rv != 0)
	{
		std::cout << "Sending data finished with an error: " << nghttp2_strerror(rv);
		return rv;
	}
	else
		std::cout << "Sending data finished OK" << std::endl;

	return 0;
}

int Session::forward_response(int32_t stream_id, short status_code, const uint8_t* data, size_t datalen)
{
	std::cout << "Forwarding response" << std::endl;

	MyDataSource data_source;
	data_source.data = data;
	data_source.dataLen = datalen;

	nghttp2_data_source dsrc;
	dsrc.ptr = &data_source;

	nghttp2_data_provider data_prd;
	data_prd.source = dsrc;
	data_prd.read_callback = &on_datasource_read;

	auto status_code_str = std::to_string(status_code);
	nghttp2_nv hdrs[] =
	{
		{(uint8_t*)":status", (uint8_t*)status_code_str.data(), 7, status_code_str.length()}
	};

	auto rv = nghttp2_submit_response(http2_session_, stream_id, hdrs, ARRLEN(hdrs), &data_prd);

	if(rv != 0)
	{
		std::cout << "Fatal error: " << nghttp2_strerror(rv) << std::endl;
		return rv;
	}

	/*if(send_response(http2_session_, stream_id, hdrs, ARRLEN(hdrs)) != 0)
	{
		return NGHTTP2_ERR_CALLBACK_FAILURE;
	}*/

	return session_send();
}

int Session::session_send()
{
	int rv;
	rv = nghttp2_session_send(http2_session_);

	if(rv != 0)
	{
		std::cerr << "Fatal error: " << nghttp2_strerror(rv);
		return -1;
	}

	return 0;
}

void Session::Start()
{
	if(send_server_connection_header() != 0
		|| session_send() != 0)
	{

		std::cerr << "Here need to delete session_data, it's not implemented" << std::endl;
		//delete_http2_session_data(session_data);
		throw std::runtime_error("Error while sending http settings");
	}

	do_read();
}

void Session::do_read()
{
	auto self(shared_from_this());
	socket_.async_read_some(boost::asio::buffer(data_, max_length),
		[this, self](boost::system::error_code ec, std::size_t length)
	{

		if(ec == boost::asio::error::eof)
		{
			std::cout << "HTTP/2 client closed the connection" << std::endl;
			return;
		}
		else if(!ec)
		{
			std::cout << "Read incoming data in size: " << length << std::endl;

			ssize_t readlen;
			readlen = nghttp2_session_mem_recv(http2_session_, (const uint8_t*)data_, length);

			if(readlen < 0)
			{
				throw std::runtime_error(std::string("Fatal error: ") + nghttp2_strerror((int)readlen));
			}

			if(session_send() != 0)
			{
				throw std::runtime_error("Error in session_send");
			}
		}
		else
		{
			std::cerr << "Error in reading: " << ec.message();
		}

		do_read();
	});
}

void Session::do_read_on_camera_socket(int32_t stream_id)
{
	auto buff = innerBuffer_.prepare(max_length);
	camera_socket_->async_read_some(buff,
		[self = shared_from_this(), stream_id](const boost::system::error_code & error, std::size_t bytes_transferred)
	{
		if(error)
		{
			std::cerr << "Error occurred while reading a response from a camera! Msg: " << error.message()
				<< ". Code: " << error.value() << std::endl;
			throw std::runtime_error("do_read_on_camera_socket()");
		}

		self->innerBuffer_.commit(bytes_transferred);

		std::cout << "returned in size: " << bytes_transferred << " data: " << (const char*)self->innerBuffer_.data().data() << std::endl;

		boost::beast::http::response_parser<boost::beast::http::string_body> http_parser;
		boost::beast::error_code ec;

		// parse a response's header
		auto header_consumed = http_parser.put(boost::asio::buffer(self->innerBuffer_.data(), self->innerBuffer_.size()), ec);

		// TODO: add @ec handling correctly. There could be an error because not enough data received
		if(ec) throw std::runtime_error("parsing HTTP 1.1 response header error");

		// parse a response's body
		auto body_consumed = http_parser.put(boost::asio::buffer(self->innerBuffer_.data() + header_consumed, self->innerBuffer_.size() - header_consumed), ec);

		if(ec) throw std::runtime_error("parsing HTTP 1.1 response body error");

		if(!http_parser.is_done() || !http_parser.is_header_done())
		{
			std::cout << "Not enough data. Waiting for more data!" << std::endl;
			return self->do_read_on_camera_socket(stream_id);
			//throw std::runtime_error("parsing HTTP 1.1 response is not done!");
		}

		//std::cout << "Data buffer after http beast parse: " << s->data_ << std::endl;


		if(self->forward_response(stream_id, http_parser.get().result_int(),
				(uint8_t*)http_parser.get().body().data(), http_parser.get().body().size()))
		{
			std::cerr << "Forwarding a response for a streamID: " << stream_id << " finished with an error" << std::endl;
		}

		self->innerBuffer_.consume(header_consumed + body_consumed);
		std::cout << "Total size consumed: " << header_consumed + body_consumed << std::endl;

		//s->send_data(stream_id, (const uint8_t*)s->data_, bytes_transferred);
	});
}

void Session::do_write(std::size_t length)
{
	auto self(shared_from_this());
	boost::asio::async_write(socket_, boost::asio::buffer(data_, length),
		[this, self](boost::system::error_code ec, std::size_t /*length*/)
	{
		if(!ec)
		{
			//do_read();
		}
	});
}


